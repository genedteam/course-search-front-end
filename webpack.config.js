/**
 * Created by kevindiem on 2/13/17.
 */
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");

const extractSass = new ExtractTextPlugin({
    filename: '[name].css'
});

module.exports = {
    entry: {
        'js/bundle': ['./babel/index.js'],
        'css/style': ['./scss/style.scss']
    },
    output: {
        path: path.resolve('./'),
        filename: '[name].js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: ['/node_modules/', '/scss/', '/css/'],
            loader: 'babel-loader',
            query: {
                presets: ['es2015']
            }
        },
        {
            test: /\.scss$/,
            include: path.resolve(__dirname, 'scss/'),
            loader: extractSass.extract({ fallback: 'style-loader', use: [ 'css-loader', 'sass-loader']})
        }]
    },
    plugins: [
        extractSass,
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
    ]
};
