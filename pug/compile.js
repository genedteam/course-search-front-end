const pug = require('pug'), fs = require('fs'), path = require('path');

try{
    
    writeFiles();
    
    if(process.argv[2] === '--watch'){
        watchFiles();
    }
    
}catch(e){
    
    console.error(e);
    process.exit();
    
}

function writeFiles(){
    const file = path.resolve(path.dirname(__filename), 'index.pug');
    const html = pug.renderFile(file);

    const stream = fs.createWriteStream(path.resolve(path.dirname(__filename), '../', 'index.html'), {flags: 'w', encoding: 'utf8', autoClose: true});

    stream.on('open', function(){
        stream.write(html, 'utf8', () => stream.close());
    });

    stream.on('close', function(){
        console.log("Successfully wrote index.html");
    });

    stream.on('error', function(e){
        console.log("Error writing index.html");
        console.log(e);
        process.exit();
    });
}

function watchFiles(){
    fs.watch(path.resolve(path.dirname(__filename)), {}, function(){
        writeFiles();
    });
}