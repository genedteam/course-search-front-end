"use strict";

const fs = require('fs');
const path = require('path');
const readLine = require('readline');
const readStream = fs.createReadStream('./departments.pug');

const rl = readLine.createInterface({input: readStream});

let departments = {};

rl.on('line', (data) => {
    
    const dept = data.match(/[\w &\-\/]+ ?(\([A-Z]{3,3}\))?$/);
    const code = data.match(/[A-Z]{3,4}/);
    
    console.log(data);
    
    if(dept && code){
        console.log(dept);
        console.log(code);
        departments[dept[0].trim()] = code[0];
        console.log(departments);
    }else{
        console.log('no dept or code');
    }
});

rl.on('close', () => {
    console.log('rl ended');
    try{
        fs.open(path.resolve('./departments.js'), 'w', '0777', (err, fd) => {
            if(err){
                console.log(err);
                process.exit(1);
            }
           fs.write(fd, JSON.stringify(departments), () => console.log('a')); 
        });
    }catch(e){
        console.log(e);
    }
});