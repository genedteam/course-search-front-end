#Isaac Samuel

import csv
import json

def find_relevant_courses():
	retval = []
	departments = []
	title = ""
	count = 0
	#Opens online courses csv as a dictionary
	with open('courses.csv') as file:
		reader = csv.DictReader(file)

		#Goes through each line in file
		for course in reader:

			#If the right line, count the number of online classes offerred since FALL 2016
			if course["Title"] != '' and course["Title"] != title:

				if count > 2:
					print retval
					retval.append({title: departments})
				
				departments = []
				count = 0;

				#Csv interprets course count as strings, with '' as zero, so we need to convert them.
				count += 0 if course["201636"] == '' else int(course["201636"])
				count += 0 if course["201703"] == '' else int(course["201703"])
				count += 0 if course["201736"] == '' else int(course["201736"])
			
			#Pulls the department name of the course
			if course["Subject"]:
				#Written in this CSV as 'ANTH 2008 - Families Structure Across Cultures', must just strip 'ANTH' and add to array
				temp = ""
				for letter in course["Subject"]:
					if letter == " ":
						break
					else:
						temp += letter
				departments.append(temp)
				
			
			if course["Title"] != '':
				title = course["Title"]

	if count > 2:
		print retval
		retval.append({title: departments})

	return retval

def insert_into_course_json(courses_to_be_inserted):
	all_courses = open("../courses.json", "r")
	course_json = json.load(all_courses)

	for each in courses_to_be_inserted:
		for online_course, online_departments in each.iteritems():
			for course in course_json:
				if course["name"] == online_course:
					course["online"] = True
					course["online_departments"] = online_departments

		output = open("../courses.json", "w")
		json.dump(course_json, output)


r = find_relevant_courses()
insert_into_course_json(r)