/* 
*constants to avoid retyping/calling jQuery function
*/
/*global $*/
/*global _*/

require('./lib/defiant.js')();

const Course = require('./templates/Course.js'),
      Areas = require('./lib/Areas.js'),
      Courses = require('./lib/Courses.js'),
      Categories = require('./lib/Categories.js'),
      CATEGORIES = require('./const/categories.js'),
      pagination = require('./lib/pagination.js');

const CATDIV = $('#categories'),
      COURSEDIV = $('#courses'),
      RESULTS = $('#results'),
      LOADING = $('#loading'),
      SEARCH = $('#searchBar'),
      WINDOW = $(window);

/*
* State
*/
let allCourses = [];
let state = {
  descModal: displayModalOrDropdown(),
  courses: []
};

$(document).ready(() => {
  
  lazyLoadFonts();

  Courses.getCourses({params: '*'});

  Areas.append();
  Categories.append($('#theCategories'));
  Categories.registerEvents(getCourseByCategory);

  $('.abs-button').on('click', toggleAbsButton);
  
  $('#advanced-search').on('show.bs.collapse', () => {
    Areas.setHeight();
    changeCarrot(true);
  });
  
  $('#advanced-search').on('hide.bs.collapse', changeCarrot)
  
  //Draw categories on window resize to adjust text and height. 
  WINDOW.on('resize', () => {
    if(RESULTS.is(':hidden')){
       Categories.append($('#theCategories'));
       Categories.registerEvents(getCourseByCategory);
    }else{
      checkModalDropdown();
    }
    
    if($('#areas').is(':visible'))
      Areas.setHeight();
    
  });
  
  $('#allCourses').on('click', appendAllCourses);
  $('#gened h1, #gened h2').on('click', returnToCategories);
  $('#back').on('click', returnToCategories);
  
  $('.close').on('click', () => {
    $('.modal-title').text('');
    $('#modal-desc').text('');
  });
  
  $('.search').on('click', searchBarSearch);
  
  $('#searchbar form').on('submit', function(e){
    e.preventDefault();
    e.stopPropagation();
    
    searchBarSearch(e);
    
    return false;
  })
  
  $('#clear').on('click', clearSearchOptions.bind(this, true));
  
  $('#areas input, #other input, #other select').on('change', function(e){
    
    if($('#results').is(':visible'))
      searchBarSearch(e);
  });

    $('#export').on('submit', function(){
      let favorites = {list:[]};
      let input = $('<input type="hidden" name="favorites"/>');
      let course, courseObj;
      
      $('#abs-button-toggle ul li').each(function(i, el){
        
        course = $(el).attr('data-course').split(' ');

        let courseObj = Courses.getCourses({
          deptNum: true, 
          dept: course[0], 
          num: course[1]}, (courses) => favorites.list.push(courses[0]), true);
      });
      
      if(favorites.list.length){
        input.attr('value', JSON.stringify(favorites));
        console.log(favorites);
        $('#export').append(input); 
      }
    
    });

    $('#export-email').on('click', function(){
      $(this).hide();
      $('#export-email-form').show();
    });

    $('#export-email-form').on('submit', function(){
      let favorites = {list:[]};
      let course, courseObj;
      let _this = $(this);
      $('#abs-button-toggle ul li').each(function(i, el){
        
        course = $(el).attr('data-course').split(' ');

        let courseObj = Courses.getCourses({
          deptNum: true, 
          dept: course[0], 
          num: course[1]}, (courses) => favorites.list.push(courses[0]), true);
      });

      $.ajax({
        method: 'POST',
        url: "https://gened.temple.edu/courseapi/pdf.php",
        data: {
          action: 'email',
          favorites: JSON.stringify(favorites)
        }
      })
      .success(function(){
        $('#abs-button-toggle #abs-button-toggle-message').append($(`<p>Success</p>`));
        _this.hide();
        $('#export-email').show();
      })
      .fail(function(){
        $('#abs-button-toggle #abs-button-toggle-message').append($(`<p>There was an error, please try again.</p>`));
        _this.hide();
        $('#export-email').show();
      });

      return false;
    });
});

function lazyLoadFonts(){
    $.ajax({
    url: 'https://fonts.googleapis.com/css?family=Open+Sans',
    beforeSend: ( xhr ) => {
      xhr.overrideMimeType('application/octet-stream');
    },
    success: (data) => {
      $('<link />', {
        'rel': 'stylesheet',
        'href': 'https://fonts.googleapis.com/css?family=Open+Sans'
      }).appendTo('head');
    }
  });
}

function displayModalOrDropdown(){
  return (WINDOW.width() <= 768);
}

function checkModalDropdown(){
  if(state.descModal != displayModalOrDropdown()){
    state.descModal = !state.descModal;
    Courses.append(Courses.courses, COURSEDIV, state.descModal);
    $(".course a").on('click', appendSyllabus);
  }
}

function changeCarrot(show){
  const text = show === true ? '&#9650;' : '&#9660;';
  
  return $('#carrot').html(text);
}

function animation(){
  $('#toggle-filter').animate({
    'font-size': '1.8em',
  }, 750, ()=>{
    $('#toggle-filter').animate({
      'font-size': '1.2em',
    }, 750);
  });
}

function toggleAbsButton(){
    $('#abs-button-toggle').toggleClass('hidden');
    
    if($('#abs-button-toggle').hasClass('hidden')){
      $('.abs-button').css('bottom', '0');
    }else{
      $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
    }
}

function returnToCategories(){
  resetSearch();
  
  RESULTS.hide();
  CATDIV.show();
  
  if($('.category').height() != $('.category').width()){ 
    Categories.append($('#theCategories'));
    Categories.registerEvents(getCourseByCategory);
  }
    
  $('#advanced-search').collapse('hide');
}

function resetSearch(){
  state.courses = [];
  COURSEDIV.html('');
  clearSearchOptions();
}

function clearSearchOptions(appendCourses){

  SEARCH.val('');
  
  $('#areas input:checked').each(function(i, el){
    $(el).prop('checked', false);
  });
  
  $('#select-dept option:first-child').prop('selected', true);
  
  $('#honors').prop('checked', false);
  
  if(appendCourses === true){
    if($('#results').is(':visible')){
      appendAllCourses();
    }
  }
}

function appendAllCourses(e, pageNum = 1){
  if(e) {
    e.preventDefault();
  
    if($(e.target || e.srcElement).attr('id') === 'allCourses'){
      clearSearchOptions();
    }
  }
  
  startCourseSearch();
  
  try{
    
    Courses.getCourses('*', (courses) =>{
      checkModalDropdown();
      Courses.append(Courses.courses, COURSEDIV, true, pageNum);

      $(".course a").on('click', appendSyllabus);
      $('.add-to-fav').on('click', appendFavorite);

      pagination.paginate($("#pagination"), Courses, 10, state.descModal, pageNum);
      
    });
    
  }catch(e){
    
    console.log(e);
    
  }
}

function getCourseByCategory(e){
  
  const category = $(this).attr('data-catName');
  
  startCourseSearch();
  
  Courses.getCourses({category: category}, (courses) => {
    Courses.append(courses, COURSEDIV);

    pagination.paginate($("#pagination"), Courses, 10, state.descModal);

    $(".course a").on('click', appendSyllabus);
    $('.add-to-fav').on('click', appendFavorite);
  });
  
}

function startCourseSearch(){
  //const collapse = $('#advanced-search');
  if(CATDIV.is('hidden'))
    return;

  CATDIV.hide();
  LOADING.show();
}

function appendFavorite(e){
    let courseDeptNum = $(this).attr('data-course').split(' ');
    
    console.log(courseDeptNum);
    let course = JSON.search({courses: Courses.courses}, `//courses[contains(dept, "${courseDeptNum[0]}") and num = ${courseDeptNum[1]}]`)[0];

    let list = $('#abs-button-toggle ul'), li = $('<li/>');
    
    if(course){
      li.attr('data-course', `${course.dept[0]} ${course.num}`);
      li.html(`<div>
                 ${course.name} | ${course.dept} ${course.num} | ${Areas.AREAS[course.area]} <strong>(${course.area})</strong> <i class="glyphicon glyphicon-remove remove-fav pointer" data-course="${course.dept[0]} ${course.num}">
              </div>`);
    }
    li.children('a').on('click', function(){});
    
    if(course && list.length){
      if(!$(`#abs-button-toggle ul li[data-course='${course.dept[0]} ${course.num}']`).length)
        list.append(li);
      
    }else if(course && !list.length){
      let ul = $('<ul/>');
      
      ul.append(li);
      
      $('#abs-button-toggle').append(ul);
    }
    
    if($('#abs-button-toggle').is(':visible')){
      $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
    }else{
      toggleAbsButton();
    }
  }

function appendSyllabus(){
  const cn = $(this).attr('data-cn');
  const cd = $(this).attr('data-cd');
  
  Courses.getCourses({
    deptNum: true,
    num: cn,
    dept: cd
  }, (courses)=>{
    if(courses[0]){
    let course = courses[0];
      $('.modal-title').text(`${course.name} - ${course.dept} ${course.num}`);
      $('#modal-desc').html(course.description);
    }else{
      $('.modal-title').text(`ERROR`);
      $('#modal-desc').text(`ERROR`);
    }
  },
  true);
}

function setCatHeight(){
  const cat = $('.category'),
      span = $('<h1/>').append('a').appendTo("#s"),
      charWidth = span.width(),
      width = cat.width(),
      charsPerRow = Math.ceil(width/charWidth);
  
  span.hide();
  
  cat.each((i, el) =>{
    $(el).height(width);
    
    if($(el).children('h1').text().length > charsPerRow){

      $(el).children('h1').css('marginTop', width/10);
      
    }else{ 
      
      $(el).children('h1').css('marginTop', width/3);
      
    }
    
  });
}

function searchBarSearch(e){
  
  startCourseSearch();

  if((Courses.allCourses == null || !Courses.allCourses.length)){
    return Courses.getCourses('*', searchBarSearch.bind(this));
  }else{
    
    let courses;
    let val = $('#searchBar').val();
    
    if(val){
      courses = doSearchBarSearch(val);
    }
    
    if(!courses) courses = Courses.allCourses;
    
    courses = doSearchOptions(courses);
       
    Courses.courses = courses;

    Courses.append(Courses.courses, COURSEDIV, state.descModal);
    $(".course a").on('click', appendSyllabus);
  }
  
  function doSearchOptions(courses){
    let c = {courses: courses};
    let filtered = [];
    let filter = '//courses[';
    let changed = false;
    
    if($('#areas input:checked').length){
      changed = true;
      $('#areas input:checked').each(function(){
        filter += `area="${$(this).attr('id')}" or `;
        
      });

      filter = filter.replace(/ or $/, ']');

      filtered = JSON.search(c, filter);
      c.courses = filtered;
    }
    
    if($('#select-dept').val()){
      changed = true;
      filtered = JSON.search(c, `//courses[dept="${$('#select-dept option:selected').val()}"]`);

      c.courses = filtered;
    }
    
    
    
    if($('#honors').is(':checked')){
      changed = true;
      filtered = JSON.search(c, `//courses[contains(name,"honors")]`);
    }
    
    return changed ? filtered : courses;
  }
  
  function doSearchBarSearch(searchVal){
      
      let c = {courses: Courses.allCourses};
      
      let courses = JSON.search(c, `//courses[contains(name,"${searchVal}")]`);

      return _.uniq(courses.concat(JSON.search(c,`//courses[contains(description,"${searchVal}")]`),
                            JSON.search(c, `//courses[code="${searchVal}"]`),
                            JSON.search(c, `//courses[area="${searchVal}"]`),
                            JSON.search(c, `//courses[contains(dept,"${searchVal}")]`)));
  }
}