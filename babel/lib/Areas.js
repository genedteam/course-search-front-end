/*global $*/
const areas = require('../const/areas.js');

module.exports = new Areas();

function Areas(){
  const _this = this;
  this.AREAS = areas;

    this.append = function appendAreas(div){
        div.children('.area-link').remove();
        const codes = Object.keys(_this.AREAS);
        codes.map(function(code){
            div.append($(`<a href="#" class="area-link" data-code="${code}"><h3><strong>(${code})</strong> ${_this.AREAS[code]}<h3></a>`));
        });
        
        return $('.area-link').on('click', function(){
            $(`#${$(this).attr('data-code')}`).prop('checked', true);

            if(!$('#advanced-search').is(':visible'))
            $('#advanced-search').collapse('show');
            
            $('#searchBarSubmit').trigger('click');
            $("<a href='#container'/>")[0].click();
        })
    }

    this.setHeight = function setAreaHeight(){
        if($('#areas').is(':visible')){
            $("label[for='GA']").height($("label[for='GW']").height());
            $("label[for='GY']").height($("label[for='GG']").height());
            $("label[for='GD']").height($("label[for='GQ']").height());
            $("label[for='GU']").height($("label[for='GS']").height());
            
            if($(window).width() > 1091){
            $('#other').height($('#areas').height());
            $('#filter-buttons').css('position', 'absolute');
            $('#filter-buttons').css('right', '15px');
            }else{
            $('#other').height('initial');
            $('#filter-buttons').css('position', 'initial');
            $('#filter-buttons').css('right', 'initial');
            }
        }else{
            return setTimeout(setAreaHeight, 10);
        }
    }
}