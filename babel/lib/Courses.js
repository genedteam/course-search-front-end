/*global $*/

const Course = require('../templates/Course.js');
const Areas = require('./Areas.js');

module.exports =  new Courses();

function Courses(){
    const _this = this;

    let _fetching = false;

    this.courses = [];

    this.allCourses = null;

    this.Course = Course;

    this.append = function appendCourses(courses, el, modal, pageNum = 1){
        let maxPerPage;
            
        if(courses.length){
            maxPerPage = courses.length > 10 ? 10 : courses.length;
            let i = pageNum > 1 ? ((pageNum - 1) * maxPerPage) : 0;
            let pages = Math.ceil(courses.length/maxPerPage);
            console.log(maxPerPage);
            console.log(pages);
            console.log(courses.length);
            console.log(i);
            let end = pages == pageNum ? courses.length : (maxPerPage * (pageNum));
            
            console.log($('#courses .course').length);
            if(!$('#courses .course').length){
                console.log('paginating');
                $('#pagination ul').pagination({
                    items: courses.length,
                    itemsOnPage: maxPerPage,
                    onPageClick: function(clickedPgNum, event){
                        _this.append(_this.courses, $('#courses'), modal, clickedPgNum);
                    }
                });
            }
            el.html("");
            
            for(i; i < end; i++){
                if(courses[i]){
                    el.append($(Course(courses[i], modal)));
                }
            }

        }else{
            el.html("");
            el.append($("<h1 class='text-center'>No Courses Found</h1>"));
        }
        
        $('.course a').on('click', function appendSyllabus(){
          const cn = $(this).attr('data-cn');
          const cd = $(this).attr('data-cd');
          
          _this.getCourses({
            deptNum: true,
            num: cn,
            dept: cd
          }, (courses)=>{
            console.log(courses);
            if(courses[0]){
            let course = courses[0];
              $('.modal-title').text(`${course.name} - ${course.dept} ${course.num}`);
              $('#modal-desc').html(course.description);
            }else{
              $('.modal-title').text(`ERROR`);
              $('#modal-desc').text(`ERROR`);
            }
          },
          true);
        });
        
        $('.add-to-fav').hover(function(){
           $(this).removeClass('glyphicon-heart-empty');
           $(this).addClass('glyphicon-heart');
        }, function(){
           $(this).removeClass('glyphicon-heart');
           $(this).addClass('glyphicon-heart-empty');
        });
        
        $('.add-to-fav').on('click', appendFavorite);
        $('body').scrollTop(0);
    };

    this.getCourses = function doCourseSearch(params = '*', cb = ()=>{}, preserveState){
        let courses = [];
        let allCourses = _this.allCourses;

        if(params === '*'){

            if(allCourses){
                if(!preserveState) _this.courses = allCourses.filter((course) => !(/honors/i).test(course.name));
                return cb(allCourses.filter((course) => !(/honors/i).test(course.name)));
            }
            
            if(!_this.fetching){
                _this.fetching = true;

                $.get('courses.json',
                    function(courses){
                    try{
                        allCourses = JSON.parse(courses).sort((a,b) => {
                            if(a.name < b.name){
                                return -1;
                            }else if(b.name < a.name){
                                return 1;
                            }
                            
                            return 0;
                        });
                    }catch(e){
                        console.log(e);
                        allCourses = courses.sort((a, b) => {
                            if(a.name < b.name){
                                return -1;
                            }else if(b.name < a.name){
                                return 1;
                            }
                            
                            return 0;
                        })
                    }
                
                    if(!preserveState) _this.courses = allCourses;
                    _this.allCourses = allCourses;
                    _this.fetching = false;
                    
                    return cb(allCourses.filter((course) => !(/honors/i).test(course.name)));
                
                    }).fail((err) => {
                        console.log(err);
                        return cb(null, err);
                    });
            }
            
        }else if(params.category){
            
            if(!_this.allCourses)
                doCourseSearch('*', (courses)=>{
                    return doCourseSearch(params, cb);
                });
            
            courses = JSON.search({courses: allCourses}, `//courses[contains(category, '${params.category}')]`).filter((course) => !(/honors/i).test(course.name));
            
        }else if(params.deptNum){
            
            if(!allCourses){
                return doCourseSearch('*', (courses)=>{
                    return doCourseSearch(params, cb);
                });
            }
            
            courses = JSON.search({courses: allCourses}, `//courses[contains(dept, "${params.dept}") and num = ${params.num}]`).filter((course) => !(/honors/i).test(course.name));
            
            console.log(params);
            console.log(allCourses);
        }
            
        if(!preserveState) _this.courses = courses;
        console.log(courses);
        console.log(_this);
        return cb(courses.filter((course) => !(/honors/i).test(course.name)));
        
        
    };
    
    function appendFavorite(e){
        let courseDeptNum = $(this).attr('data-course').split(' ');
        
        console.log(courseDeptNum);
        let course = JSON.search({courses: _this.courses}, `//courses[contains(dept, "${courseDeptNum[0]}") and num = ${courseDeptNum[1]}]`)[0];
    
        let list = $('#abs-button-toggle ul'), li = $('<li/>');
        console.log(course);
        if(course){
          li.attr('data-course', `${course.dept[0]} ${course.num}`);
          
          let liDiv = $('<div/>');
          
          liDiv.append(`${course.name} | ${course.dept} ${course.num} | ${Areas.AREAS[course.area]} <strong>(${course.area})</strong> `);
          
          let deleteButton = $(`<i class="glyphicon glyphicon-remove remove-fav pointer" data-course="${course.dept[0]} ${course.num}">`);
          
          liDiv.append(deleteButton);
          li.append(liDiv);
          
          deleteButton.on('click', function(){
            console.log('click');
            
            $(this).parent().parent().remove(); 
          
            if(!$('#abs-button-toggle ul li').length){
              $('.export').addClass('hidden');
              $('#export-email-form').addClass('hidden');
            }
            
            $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
            $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
            $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
          
          });
        
          if(course && list.length){
              console.log("course and list");
            if(!$(`#abs-button-toggle ul li[data-course='${course.dept[0]} ${course.num}']`).length)
              list.append(li);
          
          }else if(course && !list.length){
            console.log("course and no list");
            let ul = $('<ul/>');
            
            ul.append(li);
            
            ul.insertBefore('#abs-button-toggle-info');
          }else{
              console.log("no course");
          }
          
          $('#abs-button-toggle .export').removeClass('hidden');
          
          if($('#abs-button-toggle').is(':visible')){
            $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
            $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
            $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
          }else{
            toggleAbsButton();
          }
        }
        function toggleAbsButton(){
            $('#abs-button-toggle').toggleClass('hidden');
            
            if($('#abs-button-toggle').hasClass('hidden')){
              $('.abs-button').css('bottom', '0');
              $('#container').css('margin-bottom', '20px');
              $('main').css('margin-bottom', '20px');
            }else{
              $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
              $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
              $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
            }
        }
    }
}