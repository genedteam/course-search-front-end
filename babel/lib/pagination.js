/*global $*/

module.exports = {
    paginate: paginate,
    append: appendPaginationButton
}

function paginate(el, Courses, max, descModal, currentPage = 1){
  $('#pagination').html("");
  
  if(!Courses.courses.length) return;
  
  console.log(Courses.courses.length);
  const TotalPages = Math.floor(Courses.courses.length/max);
  const TotalButtons = TotalPages < 3 ? TotalPages : 3;
  
  let ol = $("<ol class='pagination'/>");
 //let i = 0;
  
  let diff = TotalPages - currentPage;
  

  let pgNum = (TotalButtons !== 3 || diff > TotalButtons) ? 1 : currentPage;
  
  if(currentPage > TotalButtons){
    pgNum = currentPage;
  }

  if(pgNum >= TotalPages - TotalButtons)
    pgNum = TotalPages - (TotalButtons - 1);
  
  if(pgNum > 1 && TotalPages > 3){
    appendPaginationButton(ol, "First", currentPage);
    appendPaginationButton(ol, "<", currentPage);
  }
  
  let i = (pgNum - 1 <= 0) ? 1 : pgNum - 1;
  let MaxButtons = pgNum + TotalButtons;
  for(i; i < MaxButtons; i++){
      if(i <= TotalPages){
        appendPaginationButton(ol, i, currentPage);
        
      }
      if(i === TotalPages) i += TotalButtons;
  }
  
  if(TotalPages > max && currentPage < TotalPages)
    appendPaginationButton(ol, '>', currentPage);
  
  
  if(currentPage < TotalPages && TotalPages > 3) 
     appendPaginationButton(ol, 'Last', currentPage);
  
  el.append(ol);
  
  return $('.pagination a').on('click', function(e){
    e.preventDefault();
    e.stopPropagation();
    
    let page = $(this).attr('data-page');
    let pageNum;
    
    if(page === 'First'){
      pageNum = 1
    }else if(page === 'Last'){
      pageNum = TotalPages;
    }else if(page === '<'){
      pageNum = currentPage > 1 ? currentPage - 1 : 1; 
    }else if(page === '>'){
      pageNum = currentPage + 1; 
    }else{
      pageNum = parseInt(page);
    }
    
    
    if(pageNum > TotalPages) return;
    
    Courses.append(Courses.courses, $('#courses'), descModal, pageNum);
    $(".course a").on('click', appendSyllabus);
    $("<a href='#container'/>")[0].click();
    console.log(pageNum);
    paginate($("#pagination"), Courses, 10, descModal, pageNum);
  });
  
  function appendSyllabus(){
    const cn = $(this).attr('data-cn');
    const cd = $(this).attr('data-cd');
    
    Courses.getCourses({
      deptNum: true,
      num: cn,
      dept: cd
    }, (courses)=>{
      if(courses[0]){
      let course = courses[0];
        $('.modal-title').text(`${course.name} - ${course.dept} ${course.num}`);
        $('#modal-desc').html(course.description);
      }else{
        $('.modal-title').text(`ERROR`);
        $('#modal-desc').text(`ERROR`);
      }
    },
    true);
  }
}



function appendPaginationButton(ol, number, currentPage){
  let li = document.createElement('li');
  let a = document.createElement('a');

  a.textContent = number;
  a.setAttribute('data-page', number);

  li.append(a);
  
  if(currentPage && number == currentPage)
    li.classList.add('active');
  
  ol.append(li);
}