/*global $*/
const imgUrl = "https://gened.temple.edu/courseapi/images/";
const categories = [
    {
        name: 'Law and Justice',
        img: imgUrl + 'law.png',
    },
    {
        name: 'Food Health and Well-Being',
        img: imgUrl + 'food.png',
    },
    {
        name: 'Cities Local and Global', 
        img: imgUrl + 'cities.png'
    }, 
    {
        name: 'Gender and Identity', 
        img: imgUrl + 'gender.png'
    }, 
    {
        name: 'Religion', 
        img: imgUrl + 'religion.png'
    }, 
    {
        name: 'Theater and Performance', 
        img: imgUrl + 'theater.png'
    }, 
    {
        name: 'Technology and Information Technology', 
        img: imgUrl + 'technology.png'
    }, 
    {
        name: 'Social Justice', 
        img: imgUrl + 'social.png'
    },
    {
        name: 'Music', 
        img: imgUrl + 'music.png'
    },
    {
        name: 'Globalization and Global Migration', 
        img: imgUrl + 'globalization.png'
    },
    {
        name: 'Environmental Issues and Sustainability', 
        img: imgUrl + 'environmental.png',
    },
    {
        name: 'Film Media and Communications', 
        img: imgUrl + 'film.png',
    },
];
const Category = require('../templates/Category.js');

module.exports = new Categories();

function Categories(){
    const _this = this;

    this.CATEGORIES = categories;

    this.append = function appendCategories(el, width){
        return Promise.resolve(((el) => {
            const ROW = "<div class='row'/>";

            el.html("<h2>Topics of Interest</h2>");
            
            let div = $(ROW);
            let delimiter;
            console.log(width);
            if(width > 960){
                delimiter = 4;
            }else if(width < 960 && width > 740){
                delimiter = 3;
            }else{
                delimiter = 2;
            }

            _this.CATEGORIES.sort((a,b) => {
                if(a.name < b.name)
                    return -1;
                if(a.name > b.name)
                    return 1;
                    
                return 0;
            })
                .map((cat, i) => {
                console.log(cat);
                if(i > 0 && i % delimiter === 0){
                    el.append(div);
                    div = $(ROW);
                }
                div.append($(Category(cat)));

                console.log($(Category(cat)));
            });
            

            el.append(div);

        })(el));
        
    }

    this.effects = {
        hover: categoryHover,
        leave: categoryLeave
    }

    this.setHeight = function setCatHeight() {
        if(!$('.category').width()){
            setTimeout(_this.setHeight, 0);
        }else{
            let cat = $('.category'),
                span = $('<h1/>').append('a').appendTo("#s"),
                charWidth = span.width(),
                width = cat.width(),
                charsPerRow = Math.ceil(width / charWidth);

            console.log($(cat[0]).width());
            span.remove();

            cat.each(function (i, el) {
                $(el).height(width);

                if ($(el).children('h1').text().length > charsPerRow) {
                    $(el).children('h1').css('marginTop', width / 10);
                } else {
                    $(el).children('h1').css('marginTop', width / 3);
                }
            });
        }
    }

    this.registerEvents = function registerCategoryEvents(click){
        const category = $('.category');
        category.on('mouseover', _this.effects.hover);
        category.on('mouseleave', _this.effects.leave);
        
        category.on('touchstart', _this.effects.hover);
        category.on('touchend', _this.effects.leave);
        category.on('touchcancel', _this.effects.leave);
        
        category.on('click', click);
    }
}

function categoryHover(e){
  $(this).css('filter', 'grayscale(80%)');
}

function categoryLeave(e){
  $(this).css('filter', 'grayscale(0)');
}