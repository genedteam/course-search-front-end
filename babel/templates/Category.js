module.exports = function Category(cat){
  return $(`<a class="nav-link pointer" href="search?category=${cat.name}"><div class='category col-xs-6 col-sm-4 col-md-3 col-lg-3' 
                 tabindex=0 aria-role='button' 
                 style='background-image: url(${cat.img})' 
                 data-catName="${cat.name}"/></a>`);
}