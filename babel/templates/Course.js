const AREAS = require('../const/areas.js');

module.exports = Course;

function Course(course, descModal){
    let options = descModal ? {toggle: 'modal', target: '#modal'}
        : {toggle: 'collapse', target: `#${course.dept[0]}-${course.num}`};
  return $(`
  <div>
    <div class="course">
        <div>
        <a data-toggle="${options.toggle}" data-target="${options.target}" data-cn=${course.num} data-cd=${course.dept}>
            <h2>${course.name}</h2>
        </a> <i class='glyphicon glyphicon-heart-empty pointer add-to-fav' data-course="${course.dept[0]} ${course.num}" alt="Click to add this course to your favorites" title="Click to add this course to your favorites"></i>
        </div>
        <span>${course.online == true  && document.getElementById("online").checked  ? course.online_departments : course.dept } ${course.num} | ${AREAS[course.area]} <strong>(${course.area})</strong></span> | ${course.online == true ? 'Online &#10004' : ''} ${course.honors == true ? 'Honors &#10004' : ''} ${course.study_abroad == true ? 'Study Abroad &#10004' : ''}
    </div>
    <div class="collapse" id="${course.dept[0]}-${course.num}">
        <h3>Course Description</h3>
        <p>${course.description}</p>
        <p>To see general availability for this course please visit <a href="http://tucourses.temple.edu">TUCourses</a>.</p>
        <p>To register for courses please visit <a href="https://tuportal.temple.edu">TUPortal</a> and see "Registration" under the "Student Tools" tab.</p>
        <b>${course.online == true ? "This course has been offered online. For future availability, please visit <a href='http://tucourses.temple.edu'>TUCourses</a>." : "" }</b><br>
        <b>${course.honors == true ? "This course has been offered as an honors course. For future availability, please visit <a href='http://tucourses.temple.edu'>TUCourses</a>." : "" }</b><br>
        <b>${course.study_abroad == true ? "This course has been offered abroad. For future availability and locations, please visit <a href='https://studyabroad.temple.edu/programs'>Explore Study Abroad Programs</a>." : "" }</b>
    </div>
  </div>`);
}

