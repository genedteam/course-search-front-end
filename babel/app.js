require('./lib/defiant.js')();

/*global $*/
/*global _*/

const Courses = require('./lib/Courses.js');
const Categories = require('./lib/Categories.js');
const Areas = require('./lib/Areas.js');
const pagination = require('./lib/pagination.js');

const app = ((window) => {
  console.log();
  return {
    routes: {
      'home' : {
        href: '/home',
        onLoad: function(event){
          $('#home').hide();
          $('#results').hide();
          $('#loading').show();

          Courses.getCourses();
          Areas.append($('#areaList'));
          Categories.append($('#theCategories'), $(window).width());
          $('#courses').html('');
          $('#pagination ul').html('');
        },
        render: renderHome
      },
      'search': {
        href:'/search',
        onLoad: function(event, queryString){
          $('#courses').html('');
          $('#pagination ul').html('');
          if(queryString)
            console.log(queryString);
          let courseQuery;
          
          if(queryString === 'all'){
            courseQuery = '*';
          }else if(/category=.*$/.test(queryString)){
            courseQuery = {category: decodeURI(queryString.split('=')[1])};
          }else if(/query=.*$/.test(queryString)){
            courseQuery = false;
          }
          
          if(courseQuery){
            Courses.getCourses(courseQuery, () => {
                Courses.append(Courses.courses, $('#courses'), ($(window).width() <= 768));
            });
          }else{
            Courses.append(Courses.courses, $('#courses'), ($(window).width() <= 768));
          }
          $('#results #sorted').text('Sorted alphabetically');
        },
        render: function(event, data){
          console.log(data);
          $('#home').hide();
          $('#results').show();
        }
      }
    }
  }

});

$('#advanced-search').on('show.bs.collapse', () => {
    Areas.setHeight();
    changeCarrot(true);
});

$('.abs-button').on('click', toggleAbsButton);

$('#clear').on('click', clearSearchOptions);
$('#back').on('click', clearSearchOptions);
$('#searchbar form').on('submit', () => {
    $('#searchBarSubmit').trigger('click')
    return false;
});

$('#export').on('submit', function() {
    let favorites = {list:[]};
    let course, courseObj;
    let _this = $(this);
    $('#abs-button-toggle ul li').each(function(i, el){
      
      course = $(el).attr('data-course').split(' ');

    let courseObj = Courses.getCourses({
      deptNum: true, 
      dept: course[0], 
      num: course[1]}, (courses) => favorites.list.push(courses[0]), true);
    });
    
    let hiddenInput = $('<input type="hidden" name="favorites">');
    
    hiddenInput.val(JSON.stringify(favorites));
    
    _this.append(hiddenInput);
});

$('#export-email').on('click', function(){
    $(this).addClass('hidden');
    $('#export-email-form').removeClass('hidden');
    $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
    $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
    $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
});

  $('#export-email-form').on('submit', function(e){
    e.preventDefault();
    e.stopPropagation();
    
    let favorites = {list:[]};
    let course, courseObj;
    let _this = $(this);
    $('#abs-button-toggle ul li').each(function(i, el){
      
      course = $(el).attr('data-course').split(' ');

    let courseObj = Courses.getCourses({
      deptNum: true, 
      dept: course[0], 
      num: course[1]}, (courses) => favorites.list.push(courses[0]), true);
    });

    console.log(favorites);
    $.ajax({
      method: 'POST',
      url: "https://gened.temple.edu/courseapi/pdf.php",
      data: {
        action: 'email',
        favorites: JSON.stringify(favorites),
        address: $('input[name="email-address"]').val()
      }
    })
    .then(function(){
      $('#abs-button-toggle #abs-button-toggle-message').append($(`<p>Success</p>`));
      _this.hide();
      $('#export-email').removeClass('hidden');
      $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
      $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
      $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
    })
    .fail(function(){
      $('#abs-button-toggle #abs-button-toggle-message').append($(`<p>There was an error, please try again.</p>`));
      _this.hide();
      $('#export-email').removeClass('hidden');
      $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
      $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
      $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
    });

    return false;
  });

function renderHome(event, data){
  
  $('#loading').hide();
  
  $('#home').show();

  Categories.setHeight();
}

function toggleAbsButton(){
    $('#abs-button-toggle').toggleClass('hidden');
    
    if($('#abs-button-toggle').hasClass('hidden')){
      $('.abs-button').css('bottom', '0');
      $('#container').css('margin-bottom', '20px');
      $('main').css('margin-bottom', '20px');
    }else{
      $('.abs-button').css('bottom', $('#abs-button-toggle').height() + 20 + 'px');
      $('#container').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
      $('main').css('margin-bottom', $('#abs-button-toggle').height() + 20 + 'px');
    }
}

function clearSearchOptions(appendCourses){

  $('#searchBar').val('');
  
  $('#areas input:checked').each(function(i, el){
    $(el).prop('checked', false);
  });
  
  $('#select-dept option:first-child').prop('selected', true);
  
  $('#honors').prop('checked', false);
  
  if(appendCourses === true){
    if($('#results').is(':visible')){
      Courses.append(Courses.allCourses, $('#courses'), ($(window).width() <= 768));
      //pagination.paginate($('#pagination'), Courses, 15, ($(window).width() <= 768), 1);
    }
  }
}

function changeCarrot(show){
  const text = show === true ? '&#9650;' : '&#9660;';
  
  return $('#carrot').html(text);
}

module.exports = app;