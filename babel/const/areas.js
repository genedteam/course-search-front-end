module.exports = {
  GW: 'Analytical Reading & Writing',
  GA: 'Arts',
  GG: 'Global/World Society',
  GY: 'Mosaic I',
  GZ: 'Mosaic II',
  GQ: 'Quantitative Literacy',
  GD: 'Race & Diversity',
  GS: 'Science & Technology',
  GU: 'U.S. Society',
  GB: 'Human Behavior'
};