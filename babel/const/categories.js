const imgUrl = "https://gened.temple.edu/courseapi/images/";
module.exports = [
    {
        name: 'Cities Local and Global', 
        img: imgUrl + 'cities.png'
    }, 
    {
        name: 'Gender and Identity', 
        img: imgUrl + 'gender.png'
    }, 
    {
        name: 'Religion', 
        img: imgUrl + 'religion.png'
    }, 
    {
        name: 'Theater and Performance', 
        img: imgUrl + 'theater.png'
    }, 
    {
        name: 'Technology and Information Technology', 
        img: imgUrl + 'technology.png'
    }, 
    {
        name: 'Social Justice', 
        img: imgUrl + 'social.png'
    },
    {
        name: 'Music', 
        img: imgUrl + 'music.png'
    },
    {
        name: 'Globalization and Global Migration', 
        img: imgUrl + 'globalization.png'
    },
    {
        name: 'Environmental Issues and Sustainability', 
        img: imgUrl + 'environmental.png'
    },
    {
        name: 'Film Media and Communications', 
        img: imgUrl + 'film.png'
    },
    {
        name: 'Law and Justice',
        img: ''
    },
    {
        name: 'Food Health and Well-Being',
        img: ''
    }
];