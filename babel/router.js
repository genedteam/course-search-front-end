/*global $*/
/*global history*/

const router = ((window) => { 
    const app = require('./app.js')(window);

    return function router(route, event, thisArg = window){

        $('#404').remove();

        //ignore query string
        const url = route.url.replace(/\?[A-Za-z0-9]*$/, '');

        if(app.routes[url] && app.routes[url].href){
            console.log('f');
            const onload = app.routes[url].onLoad && typeof(app.routes[url].onLoad) === 'function';
            const render = app.routes[url].render && typeof(app.routes[url].render) === 'function';

            if(onload && render){
            
                Promise.resolve(app.routes[url].onLoad.bind(thisArg, event, route.queryString).apply())
                    .then((data) => app.routes[url].render.bind(thisArg, event, data).apply());
            
            }else if(onload){

                app.routes[url].onLoad.bind(thisArg, event, route.queryString).apply();
            
            }else if(render){
            
                app.routes[url].render.bind(thisArg, event).apply();
            
            }
            $('body').scrollTop(0);

        }else{
            $('#app a').hide();
            $('#app').append($('<div id="404">Not Found (404) :\'( <a id="back" href="#">back</a></div>'));

            $('#back').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                history.back();
            });
        }
    }
});

module.exports = router;