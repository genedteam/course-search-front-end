'use strict';

/*global $*/
/*global history*/
/*global _*/

const Courses = require('./lib/Courses.js');

$(document).ready(function documentReady(){
  if(detectIE()){
    $('#body').html("<h1>Sorry, the GenEd Course Search doesn't support Internet Explorer.</h1><h2>Please use Microsoft Edge, Mozilla Firefox, or Google Chrome</h2>");
    return;
  }
  if(!$(window).width()){
    setTimeout(documentReady, 0);
  }else{
    const app = require('./app.js')(window);
    const router = require('./router.js')(window);
    
    lazyLoadFonts();

    $('#home').hide();
    $('#results').hide();

    let state = {};
    state.url = 'home';
    
    if(!app.routes[state.url]){
      state.url = 'home';
    }
    
    history.pushState(state, "Home", state.url);
    router(state);

    window.onpopstate = function(e){
      if(e.state){
        router(e.state, e);
      }
    };

    $(document).on('click', '.nav-link', function(e){
      e.preventDefault();

      const id = $(this).attr('id');
      const url = $(this).attr('href');
      
      const urlParams = url.split('?');
      
      let state = {url: urlParams[0], queryString: urlParams[1]};
      
      history.pushState(state, id, url);

      console.log(state);
      router(state, e);
      
    });
    
    $('.search').on('click', function(e){
      searchBarSearch(e);
    });
    

  function searchBarSearch(e){
    e.preventDefault();

    //startCourseSearch();
  
    if((Courses.allCourses == null || !Courses.allCourses.length)){
      return Courses.getCourses('*', searchBarSearch.bind(this));
    }else{
      
      let courses;
      let val = $('#searchBar').val();
      
      if(val){
        courses = doSearchBarSearch(val);
      }
      
      if(!courses) courses = Courses.allCourses;
      
      courses = doSearchOptions(courses);
         
      Courses.courses = courses;
  
      Courses.append(Courses.courses, $('#courses'), ($(window).width() <= 768));
      $(".course a").on('click', appendSyllabus);
      
      
    }
  }
  
  function appendSyllabus(){
    const cn = $(this).attr('data-cn');
    const cd = $(this).attr('data-cd');
    
    Courses.getCourses({
      deptNum: true,
      num: cn,
      dept: cd
    }, (courses)=>{
      if(courses[0]){
      let course = courses[0];
        $('.modal-title').text(`${course.name} - ${course.dept} ${course.num}`);
        $('#modal-desc').html(course.description);
      }else{
        $('.modal-title').text(`ERROR`);
        $('#modal-desc').text(`ERROR`);
      }
    },
    true);
  }
  
    function doSearchOptions(courses){
      let c = {courses: courses};
      let filtered = [];
      let filter = '//courses[';
      let changed = false;
      
      if($('#areas input:checked').length){
        changed = true;
        $('#areas input:checked').each(function(){
          filter += `area="${$(this).attr('id')}" or `;
          console.log(filter);
        });
  
        filter = filter.replace(/ or $/, ']');
  
        filtered = JSON.search(c, filter);
        c.courses = filtered;
      }
      
      if($('#select-dept').val()){
        changed = true;
        filtered = JSON.search(c, `//courses[dept="${$('#select-dept option:selected').val()}"]`);
  
        c.courses = filtered;
      }
      
      
      //Honors filter;
      if($('#honors').is(':checked')){
        changed = true;
        filtered = JSON.search(c, `//courses[honors]`);
      }
      
      if(changed && !$('#honors').is(':checked')){
        filtered = filtered.filter((course) => !(/honors/i).test(course.name));  
      }else if(!$('#honors').is(':checked')){
        courses = courses.filter((course) => !(/honors/i).test(course.name));
      }
      
  

      //Online classes
      if($('#online').is(':checked')){
        changed = true;
	if (!$('#honors').is(':checked')) {      
		filtered = JSON.search(c, `//courses[online]`);
	}
	else {
		filtered = filtered.concat(JSON.search(c, '//courses[online]'));
        }
      }


      //Study abroad classes
      if($('#study_abroad').is(':checked')){
        changed = true;
        if (!$('#study_abroad').is(':checked')) {      
          filtered = JSON.search(c, `//courses[study_abroad]`);
        }
        else {
          filtered = filtered.concat(JSON.search(c, '//courses[study_abroad]'));
        }
      }
      
      
      return changed ? filtered : courses;
    }

    function doSearchBarSearch(searchVal){
        
        let c = {courses: Courses.allCourses};
        
        let courses = JSON.search(c, `//courses[contains(name,"${searchVal}")]`);
        
        let testArr = searchVal.split(' ');
        let deptNum = {dept: '', num: ''};
        let deptCourses = [];
        

  
       if (testArr.length === 2) { 
         if(parseInt(testArr[1])){
	  
	  console.log("Searching department AND num")

	  if(testArr[1].length === 3){
            deptNum.num = '0' + testArr[1];
          }
	  else{
            deptNum.num = testArr[1];
          }

	  console.log("dept_num: " + deptNum.num);
          
          if(testArr[0].length > 4){
            const departments = require('./const/departments.js');
            
            if(departments[testArr]){
              deptNum.dept = departments[testArr];
            }
          }else{
            deptNum.dept = testArr[0];
          }

	  console.log("Department: " + deptNum.dept)
          
          try{
            deptCourses = JSON.search(c, `//courses[contains(dept, "${deptNum.dept}") and num = ${deptNum.num}]`);
            console.log('//courses[contains(dept, "${deptNum.dept}") and num = ${deptNum.num}]');
          }catch(e){
            console.log(e);
            deptCourses = [];
            console.log('//courses[contains(dept, "${deptNum.dept}") and num = ${deptNum.num}]');
          }
        }
      }

	//If searching for a course number, but there wasn't a 0 prepended, prepend the 0 so it matches with saved courses
	if (parseInt(searchVal)) {
	  if (searchVal.length == 3 ) {
	    searchVal = "0" + searchVal
	  }
	}
  
        return _.uniq(courses.concat(deptCourses,
                              JSON.search(c,`//courses[contains(description,"${searchVal}")]`),
                              JSON.search(c, `//courses[code="${searchVal}"]`),
                              JSON.search(c, `//courses[area="${searchVal}"]`),
                              JSON.search(c, `//courses[contains(dept,"${searchVal}")]`),
			      JSON.search(c, `//courses[num="${searchVal}"]`)
			      ));
    }
  }

  function lazyLoadFonts(){
      $.ajax({
      url: 'https://fonts.googleapis.com/css?family=Open+Sans',
      beforeSend: ( xhr ) => {
        xhr.overrideMimeType('application/octet-stream');
      },
      success: (data) => {
        $('<link />', {
          'rel': 'stylesheet',
          'href': 'https://fonts.googleapis.com/css?family=Open+Sans'
        }).appendTo('head');
      }
    });
  }
  
});

function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
  
  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
  
  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
  
  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  // other browser
  return false;
}
