#Goes to study abroad website, scrapes all the geneds, adds them to JSON
#Isaac Samuel

import json
from bs4 import BeautifulSoup
from urllib.request import urlopen

with open("../courses.json") as data_file:
	course_data = json.load(data_file)


#ROME:
WEBSITE_URLS = ['https://studyabroad.temple.edu/courses/temple-rome-semester?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value']
#ROME SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-rome-undergraduate-summer?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#BRAZIL SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-brazil?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#SPAIN SPRING
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-in-spain-spring-semester?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#SPAIN SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-in-spain-summer?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#INDIA SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-india?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#GHANA SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-ghana?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#JAMAICA SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-jamaica?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#FRANCE
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-france?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#GERMANY
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-germany?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#UK Global Culture
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-the-uk-english-culture?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#ITALY SUMMER
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-italy?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#NORTHERN IRELAND CULTURE
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-northern-ireland-irish-culture?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")
#KOREA GLOBAL MOSIAC
WEBSITE_URLS.append("https://studyabroad.temple.edu/courses/temple-summer-in-korea-global-mosaic?field_department_value=&field_semesters_offered__tid=All&title=&field_course_theme_tid=All&sort_bef_combine=field_course_number_value%20ASC&sort_order=ASC&sort_by=field_course_number_value")

#Starting with the list of geneds offerred in Japan
geneds = ["The Art of Acting", "Sport & Leisure in American Society", "Asian Behavior & Thought", "Brain Matters", "World Affairs", "Development & Globalization", "The Making of American Society: Melting Pot or Culture Wars?", "The History & Significance of Race in America", "Exploring the Cosmos", "Exploring Music", "World Musics & Cultures", "Mathematical Patterns", "Math for a Digital World", "Data Science", "Dissent in America", "Representing Race", "Politics of Identity in America", "Creative Acts", "Language in Society", "The American Economy", "Cyberspace & Society", "DNA: Friend or Foe", "Arts in Cultural Context", "Nature Has No Reverse", "Politics of Identity in America", "Dissent in America", "African Americans, Equality and the Law", "Philosophy of the Human", "Exploring Music", "World Musics & Cultures", "Human Sexuality", "Quantitative Methods in the Social Sciences", "Youth Cultures", "Statistics in the News"]

for url in WEBSITE_URLS:
  html = urlopen(url).read()
  soup = BeautifulSoup(html, "lxml")
  
  tables = soup.find_all("tbody")
  for table in tables:
    course_numbers = table.find_all(attrs={"data-title":"Course Number"})
    course_names = table.find_all(attrs={"data-title":"Course Title"})
    
    count = 0
    
    while count < len(course_numbers):
      course_num = course_numbers[count].contents[0].strip()
      if len(course_num) > 4:
        course_num = int(course_num[0:4])
      else:
        course_num = int(course_num)

        if course_num > 800 and course_num < 999:
          if course_names[count].a.contents[0] not in geneds:
            geneds.append(course_names[count].a.contents[0])
      count += 1




for course in course_data:
  for key, value in course.copy().items():
    for gened in geneds:
      course["study_abroad"] = False
      if gened == value:
        course["study_abroad"] = True
        geneds.remove(gened)

#Manually input the ones which are printed out
print(geneds)


with open("../courses.json", "w+") as output:
	json.dump(course_data, output)
