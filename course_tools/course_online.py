#Takes a list of online course and sets their online status to true
#Sets all other course's online status to false (can comment this out)

import json
import codecs
import os

#These are for 2018/2019 year
COURSES = [
"Advert & Globalization",
"American Revolutions",
"Analytical Reading & Writing",
"Arts in Cultural Context",
"Asian Behavior & Thought",
"Bilingual Communities",
"Brain Matters",
"Classics Af Am Theater",
"Confronting Empire",
"Creative Acts",
"Creativity and Innovation",
"Development & Globalization",
"Digital Mapping",
"Dimensions of Diversity",
"Dramatic Imagination",
"Education for Liberation",
"Evolution of Culture",
"Exploring Music",
"First Person America",
"Founding Philadelphia",
"Gender & World Societies",
"Global Slavery",
"Green vs Gray: Urban Economy",
"Hist Signif Race in Amer",
"Human Ecology",
"Human Sexuality",
"IH I: The Good Life",
"IH II: The Common Good",
"Imaginary Cities",
"Immigration & Amer Dream",
"Immigration & American Dream",
"Investing for the Future",
"Landscape Amer Thought",
"Language in Society",
"Law & American Society",
"Living for Change",
"Making Amer Society",
"Mathematical Patterns",
"Mosaic: Humanities Sem I",
"Mosaic: Humanities Sem II",
"Politics of Identity",
"Quant Methods in Soc Sci",
"Race & Poverty in the Americas",
"Race, Identity & Experience",
"Race/Ethnicity in Cinema",
"Religion in the World",
"Representing Race",
"Sacred Space",
"Shall We Dance?",
"Sport & Leisure in American Society",
"Sustainable Design",
"Sustainable Environments",
"The American Economy",
"The Creative Spirit",
"The Detective Novel",
"The Future of Your TV",
"The Global Crisis",
"The Meaning of Madness",
"The Meaning of the Arts",
"Tweens and Teens",
"War and Peace",
"Workings of the Mind",
"World Affairs",
"World Musics",
"World Regions & Cultures",
"World Society in Lit & Film",
"Youth Cultures"
]


with open("../courses.json") as data_file:
	data = json.load(data_file)
print len(COURSES)

i=0
while i < len(data):
	data[i]["online"] = False #Sets to false by default--can comment this out
	
	for course in COURSES:
		if i < len(data) and data[i]["name"] == course:
			data[i]["online"] = True
			COURSES.remove(course)
		i += 1

print COURSES #Prints any course that could not be found... You must change these manually

print len(COURSES)

with open("courses.json", "w+") as output:
	json.dump(data, output)
