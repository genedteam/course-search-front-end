#Should only be used once--collating the honors courses to be in same format as online courses

import json
import codecs
import os


with open("../courses.json") as data_file:
	data = json.load(data_file)

for each in data:
	i = 0
	name = each["name"]
	while i < len(data):
		if data[i]["name"] == "Honors " + name:
			each["honors"] = True
			del data[i]
		i += 1


with open("courses.json", "w+") as output:
	json.dump(data, output)
