#Deletes courses from the courses.json fileHonors Why care about College: Higher Education in American Life

import json
import codecs
import os

NAME_OF_COURSE_TO_BE_DELETED = "Honors Art of Acting"


with open("../courses.json") as data_file:
	data = json.load(data_file)

i=0
while i < len(data):
	if data[i]["name"] == NAME_OF_COURSE_TO_BE_DELETED:
		del data[i]
	i += 1


with open("../courses.json", "w+") as output:
	json.dump(data, output)
