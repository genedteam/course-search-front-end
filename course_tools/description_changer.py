# -*- coding: utf-8 -*-

#Changes the description and categorty in the courses.json file

#Environmental Issues and Sustainability
#Theater and Performance
#Cities: Local and Global
#Globalization and Global Migration
#Film Media and Communications
#Food Health and Well-Being
#Technology and Information Technology
#Social Justice
#Law and Justice
#Gender and Identity

import json
import codecs
import os

DESCRIPTION = "You have decided to go to college.  But why? What role will college and in particular Temple University play in your life?  Reflect on this important question by looking at the relationship between higher education and American society.  What do colleges and universities contribute to our lives?  They are, of course, places for teaching and learning.  They are also research centers, sports and entertainment venues, sources of community pride and profit, major employers, settings for coming-of-age rituals (parties, wild times, courtship, etc.), and institutions that create lifetime identities and loyalties.  Learn how higher education is shaped by the larger society and how, in turn, it has shaped that society. Become better prepared for the world in which you have chosen to live for the next few years."
COURSE_CATEGORY = ["Food Health and Well-Being"]
COURSE_NAME = "Why Care About College: Higher Education in American Life"

with open("../courses.json") as data_file:
	data = json.load(data_file)

name_found = False
num_found = False
found = False
for each in data:
	for key, value in each.items():
		if key == "name" and value == COURSE_NAME:
			name_found = True


		if name_found:
			each["description"] = DESCRIPTION
			each["category"] = COURSE_CATEGORY
			found = True 

			num_found = False
			name_found = False

if not found:
	print("Course name + num not found.")

with open("../courses.json", "w+") as output:
	json.dump(data, output)
