(function(){
  var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

  if(width <= 960) window.location = "https://gened.temple.edu/coursesearch";

  $(document).ready(function(){
    $('.main').remove();
    $("<div style='height: 100vh; width: 100vw;'>"+
	"<iframe style='border: none;' width='100%' height='100%' src='https://gened.temple.edu/coursesearch'></iframe>"+
      "</div>"
     ).insertAfter('.header');
     });
  }());
